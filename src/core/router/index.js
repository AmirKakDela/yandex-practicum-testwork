import {createRouter, createWebHashHistory} from 'vue-router';
import PostDetail from '../../components/Common/PostDetail.vue';
import UserProfile from '../../components/UserProfile/UserProfile.vue';
import TaskWithStart from '../../components/TaskWithStar/TaskWithStar.vue';

const routes = [
    {
        path: '/',
        name: 'UserProfile',
        component: UserProfile
    },
    {
        path: '/posts/:id',
        name: 'PostDetail',
        component: PostDetail
    },
    {
        path: '/task-with-star',
        name: 'TaskWithStar',
        component: TaskWithStart
    }
];
export const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export const user = {
    nickname: 'amir1',
    name: 'Амир Ахметжанов | Frontend Developer at MOEX',
    hasStories: true,
    profileDescription: 'Ссылка на репозиторий этого проекта: ',
    ownLink: 'https://gitlab.com/AmirKakDela/yandex-practicum-testwork',
    followers: 560,
    following: 204,
    postsCount: 5,
    avatar: {
        src: 'images/im4.png',
        alt: 'Фото пользователя amir1'
    },
}

export const posts = [
    {
        id: 1,
        src: 'images/why_program.jpg',
        likes: 103,
        type: 'photo',
        comments: 0,
        text: ' Я люблю код. Я люблю давать команды программе, компьютеру или любому другому устройству. Мне нравится четкость, с которой исполняется код.  Мне нравится рациональность и немного творчества. А иногда и много творчества :) <br><br>' +
            'Я люблю программирование уже более 7 лет. Еще в 9 классе, когда стал выбор что сдавать на ОГЭ - я выбрал информатику. Pascal, "Черепашка", HTML/CSS/JS. Всё это сразу же мне приглянулось и понравилось. Мне было очень легко готовиться к ОГЭ по информатике, поскольку это приносило удовольствие и не напрягало. В 10-11 классе я уже понимал, что сдав ЕГЭ, я поступлю на программиста. Тогда я увлекся созданием сайтов и до сих пор не смог оторваться!) Помимо фронтенд-разработки мне точно так же нравится писать и бэк.'
    },
    {
        id: 2,
        src: 'images/gap.png',
        video: 'images/last_done.mp4',
        likes: 201,
        type: 'video',
        comments: 0,
        text: 'Сегодня я расскажу Вам, что такое gap, как его использовать и какие проблемы он решает.'
    },
    {
        id: 3,
        src: 'images/im1.jpg',
        likes: 120,
        type: 'photo',
        comments: 0,
        text: 'Hello world!'
    },
    {
        id: 4,
        src: 'images/im2.jpg',
        likes: 72,
        type: 'photo',
        comments: 0,
        text: 'Моё селфи. Стоп, или мой селфи? :)'
    },
    {
        id: 5,
        src: 'images/im3.jpg',
        likes: 84,
        type: 'photo',
        comments: 0,
        text: 'Июль 2023. Я в парке. Гулял с друзьями :)'
    },
    {
        id: 6,
        src: 'images/im4.jpg',
        likes: 84,
        type: 'photo',
        comments: 0,
        text: ''
    }
]

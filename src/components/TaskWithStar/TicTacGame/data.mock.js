export const getBlocks = () => Array.from(new Array(9), (item, i) => ({id: i + 1, selectedType: ""}))
export const winCombinations = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]];

export const TIC = 'TIC'; // крестик
export const TAC = 'TAC'; // нолик

export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}